#![allow(confusable_idents, mixed_script_confusables)]  // for Greek letters

pub mod utils;
pub mod day1;
pub mod day2;
pub mod day3;
pub mod day4;
pub mod day5;
pub mod day6;
