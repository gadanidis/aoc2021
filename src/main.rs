use aoc2021::day1;
use aoc2021::day2;
use aoc2021::day3;
use aoc2021::day4;
use aoc2021::day5;
use aoc2021::day6;
use aoc2021::utils::timed_solution;

fn main() {
    timed_solution(1, day1::part1, day1::part2);
    timed_solution(2, day2::part1, day2::part2);
    timed_solution(3, day3::part1, day3::part2);
    timed_solution(4, day4::part1, day4::part2);
    timed_solution(5, day5::part1, day5::part2);
    timed_solution(6, day6::part1, day6::part2);
}
