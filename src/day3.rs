use crate::utils::get_input;
use grid::*;
use itertools::Itertools;

fn input_to_grid(input: &str) -> Grid<char> {
    let rows = input.trim().lines().map(|x| x.chars().collect());
    let mut grid = grid![];
    for row in rows {
        grid.push_row(row);
    }

    grid
}

fn rate(grid: &Grid<char>, compare: fn(usize, usize) -> bool) -> u32 {
    let mut rate = "".to_string();
    let (col_length, row_length) = grid.size();

    for col in 0..row_length {
        let n_zeroes = grid.iter_col(col).filter(|x| **x == '0').count();
        if compare(n_zeroes, col_length / 2) {
            rate.push('0')
        } else {
            rate.push('1')
        }
    }

    u32::from_str_radix(&rate, 2).unwrap()
}

fn γ(grid: &Grid<char>) -> u32 {
    rate(grid, |x, y| x > y)
}

fn ε(grid: &Grid<char>) -> u32 {
    rate(grid, |x, y| x < y)
}

pub fn part1() -> u32 {
    let input = get_input("day3.txt");
    let grid = input_to_grid(&input);
    γ(&grid) * ε(&grid)
}

fn gas_rating(grid: &Grid<char>, keep_char: fn(usize, usize) -> char) -> u32 {
    let row_length = grid.cols();
    let mut candidates: Vec<Vec<char>> = grid
        .iter()
        .chunks(row_length)
        .into_iter()
        .map(|x| x.copied().collect())
        .collect();

    let mut result_grid = grid.clone();

    for col in 0..row_length {
        let n_zeroes = result_grid.iter_col(col).filter(|x| **x == '0').count();
        let n_ones = result_grid.iter_col(col).filter(|x| **x == '1').count();

        let keep = keep_char(n_zeroes, n_ones);
        candidates = candidates
            .iter()
            .filter(|x| x[col] == keep)
            .cloned()
            .collect();
        result_grid.clear();
        for c in candidates.iter() {
            result_grid.push_row(c.to_vec());
        }

        if candidates.len() == 1 {
            break;
        }
    }

    u32::from_str_radix(
        candidates[0]
            .clone()
            .into_iter()
            .collect::<String>()
            .as_str(),
        2,
    )
    .unwrap()
}

fn co2(grid: &Grid<char>) -> u32 {
    gas_rating(grid, |zeroes, ones| if zeroes > ones { '1' } else { '0' })
}

fn o2(grid: &Grid<char>) -> u32 {
    gas_rating(grid, |zeroes, ones| if ones >= zeroes { '1' } else { '0' })
}

pub fn part2() -> u32 {
    let input = get_input("day3.txt");
    let grid = input_to_grid(&input);
    co2(&grid) * o2(&grid)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn sample_grid() -> Grid<char> {
        input_to_grid(
            "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010",
        )
    }

    #[test]
    fn ε_test() {
        assert_eq!(ε(&sample_grid()), 9)
    }

    #[test]
    fn γ_test() {
        assert_eq!(γ(&sample_grid()), 22)
    }

    #[test]
    fn o2_test() {
        assert_eq!(o2(&sample_grid()), 23)
    }

    #[test]
    fn co2_test() {
        assert_eq!(co2(&sample_grid()), 10)
    }
}
