use crate::utils::get_input;

pub fn part1() -> u32 {
    let input = get_input("day1.txt");
    let measurements = input
        .trim()
        .lines()
        .map(|x| x.parse::<u32>().unwrap());

    let mut last = None;
    let mut n_increases = 0;

    for m in measurements {
        if let Some(l) = last {
            if m > l {
                n_increases += 1;
            }
        }
        last = Some(m);
    }

    n_increases
}

pub fn part2() -> u32 {
    let input = get_input("day1.txt");
    let measurements = input
        .trim()
        .lines()
        .map(|x| x.parse::<u32>().unwrap());

    let mut last = (None, None, None);
    let mut n_increases = 0;

    for m in measurements {
        if last.0.is_some() && last.1.is_some() && last.2.is_some() {
            let this_sum = m + last.0.unwrap() + last.1.unwrap();
            let last_sum = last.0.unwrap() + last.1.unwrap() + last.2.unwrap();

            if this_sum > last_sum {
                n_increases += 1;
            }
        }
        last.2 = last.1;
        last.1 = last.0;
        last.0 = Some(m);
    }

    n_increases
}
