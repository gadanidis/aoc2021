use crate::utils::get_input;

enum Move {
    Up(u32),
    Down(u32),
    Forward(u32),
}

impl Move {
    fn from_str(string: &str) -> Self {
        let parts = string.split(' ').collect::<Vec<&str>>();
        let dist = parts[1].parse().unwrap();

        match parts[0] {
            "forward" => Self::Forward(dist),
            "down" => Self::Down(dist),
            "up" => Self::Up(dist),
            _ => panic!("oops!"),
        }
    }
}

struct Pos {
    x: u32,
    y: u32,
}

impl Pos {
    fn new() -> Self {
        Pos { x: 0, y: 0 }
    }

    fn update(&mut self, m: Move) {
        match m {
            Move::Up(n) => self.y -= n,
            Move::Down(n) => self.y += n,
            Move::Forward(n) => self.x += n,
        }
    }
}

struct Pos2 {
    x: u32,
    y: u32,
    aim: u32,
}

impl Pos2 {
    fn new() -> Self {
        Pos2 { x: 0, y: 0, aim: 0 }
    }

    fn update(&mut self, m: Move) {
        match m {
            Move::Up(n) => self.aim -= n,
            Move::Down(n) => self.aim += n,
            Move::Forward(n) => {
                self.x += n;
                self.y += self.aim * n
            }
        }
    }
}

fn get_moves() -> Vec<Move> {
    let input = get_input("day2.txt");
    input.trim().lines().map(Move::from_str).collect()
}

pub fn part1() -> u32 {
    let moves = get_moves();
    let mut pos = Pos::new();

    for m in moves {
        pos.update(m);
    }

    pos.x * pos.y
}

pub fn part2() -> u32 {
    let moves = get_moves();
    let mut pos = Pos2::new();

    for m in moves {
        pos.update(m);
    }

    pos.x * pos.y
}
