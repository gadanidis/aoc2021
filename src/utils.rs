use std::fmt::Display;
use std::fs;
use std::time;

pub fn get_input(path: &str) -> String {
    fs::read_to_string(env!("CARGO_MANIFEST_DIR").to_string() + "/input/" + path).unwrap()
}

pub fn timed_solution<T: Display>(day: u8, part1: fn() -> T, part2: fn() -> T) {
    let now = time::Instant::now();
    let part1_solution = part1();
    println!(
        "{}:1 took {} ms and returned: {}",
        day,
        now.elapsed().as_millis(),
        part1_solution
    );
    let now = time::Instant::now();
    let part2_solution = part2();
    println!(
        "{}:2 took {} ms and returned: {}",
        day,
        now.elapsed().as_millis(),
        part2_solution
    );
}
