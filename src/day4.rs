use crate::utils::get_input;
use grid::*;

#[derive(Clone, Debug)]
struct BingoBoard {
    grid: Grid<u32>,
}

impl BingoBoard {
    fn from_grid(grid: Grid<u32>) -> Self {
        BingoBoard { grid }
    }

    fn wins(&self, nums: &[u32]) -> bool {
        for row in 0..self.grid.rows() {
            if self.grid.iter_row(row).all(|x| nums.contains(x)) {
                return true;
            }
        }
        for col in 0..self.grid.cols() {
            if self.grid.iter_col(col).all(|x| nums.contains(x)) {
                return true;
            }
        }
        false
    }

    fn score(&self, nums: Vec<u32>, last_called: u32) -> u32 {
        let uncalled_sum: u32 = self.grid.iter().filter(|x| !nums.contains(x)).sum();
        uncalled_sum * last_called
    }
}

fn process_input() -> (Vec<u32>, Vec<BingoBoard>) {
    let input = get_input("day4.txt");
    let mut pieces = input.split("\n\n");
    let numbers = pieces
        .next()
        .unwrap()
        .split(',')
        .map(|x| x.parse::<u32>().unwrap())
        .collect();

    let mut boards = vec![];

    for board in pieces {
        let mut grid = grid![];
        for r in board.lines() {
            grid.push_row(
                r.split_whitespace()
                    .map(|x| x.parse::<u32>().unwrap())
                    .collect(),
            );
        }
        boards.push(BingoBoard::from_grid(grid));
    }

    (numbers, boards)
}

pub fn part1() -> u32 {
    let (numbers, boards) = process_input();
    let mut called = vec![];

    for n in numbers {
        called.push(n);
        for b in &boards {
            if b.wins(&called) {
                return b.score(called, n);
            }
        }
    }
    panic!()
}

pub fn part2() -> u32 {
    let (numbers, boards) = process_input();
    let mut called = vec![];
    let mut boards_left = boards;

    for n in numbers {
        called.push(n);
        if boards_left.len() == 1 && boards_left[0].wins(&called) {
            return boards_left[0].score(called, n);
        }
        boards_left = boards_left
            .iter()
            .filter(|x| !x.wins(&called))
            .cloned()
            .collect();
    }
    panic!()
}
