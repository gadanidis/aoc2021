use crate::utils::get_input;
use std::collections::HashMap;

#[derive(PartialEq, Eq, Debug, PartialOrd)]
struct Point((u32, u32));

impl Point {
    fn new(x: u32, y: u32) -> Self {
        Point((x, y))
    }

    fn x(&self) -> u32 {
        self.0 .0
    }

    fn y(&self) -> u32 {
        self.0 .1
    }
}

#[derive(PartialEq, Debug)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn from_str(st: &str) -> Self {
        let mut parts = st
            .split(" -> ")
            .map(|x| x.split(','))
            .flatten()
            .map(|x| x.parse().unwrap());
        let x1 = parts.next().unwrap();
        let y1 = parts.next().unwrap();
        let x2 = parts.next().unwrap();
        let y2 = parts.next().unwrap();

        Line {
            start: Point::new(x1, y1),
            end: Point::new(x2, y2),
        }
        .sorted()
    }

    fn sorted(self) -> Self {
        if self.start < self.end {
            self
        } else {
            Line {
                start: self.end,
                end: self.start,
            }
        }
    }

    fn is_vertical(&self) -> bool {
        self.start.x() == self.end.x()
    }

    fn is_horizontal(&self) -> bool {
        self.start.y() == self.end.y()
    }

    fn xrange(&self) -> Box<dyn Iterator<Item = u32>> {
        if self.start.x() < self.end.x() {
            Box::new(self.start.x()..=self.end.x())
        } else {
            Box::new((self.end.x()..=self.start.x()).rev())
        }
    }

    fn yrange(&self) -> Box<dyn Iterator<Item = u32>> {
        if self.start.y() < self.end.y() {
            Box::new(self.start.y()..=self.end.y())
        } else {
            Box::new((self.end.y()..=self.start.y()).rev())
        }
    }

    fn points(&self) -> Vec<(u32, u32)> {
        if self.is_vertical() {
            self.yrange().map(|y| (self.start.x(), y)).collect()
        } else if self.is_horizontal() {
            self.xrange().map(|x| (x, self.start.y())).collect()
        } else {
            self.xrange()
                .zip(self.yrange())
                .map(|(x, y)| (x, y))
                .collect()
        }
    }
}

fn get_lines_from_input(orthogonal_only: bool) -> Vec<Line> {
    let input = get_input("day5.txt");
    let lines = input.trim().lines().map(Line::from_str);
    if orthogonal_only {
        lines
            .filter(|x| x.is_horizontal() || x.is_vertical())
            .collect()
    } else {
        lines.collect()
    }
}

pub fn part1() -> usize {
    let lines: Vec<Line> = get_lines_from_input(true);
    let mut points = HashMap::new();

    for line in lines {
        for point in line.points() {
            let p = points.entry(point).or_insert(0);
            *p += 1;
        }
    }
    points.values().filter(|x| x >= &&2).count()
}

pub fn part2() -> usize {
    let lines: Vec<Line> = get_lines_from_input(false);
    let mut points = HashMap::new();

    for line in lines {
        for point in line.points() {
            let p = points.entry(point).or_insert(0);
            *p += 1;
        }
    }
    points.values().filter(|x| x >= &&2).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sort_test() {
        let sample = Line::from_str("9,7 -> 7,7");
        assert_eq!(
            Line {
                start: Point::new(7, 7),
                end: Point::new(9, 7)
            },
            sample
        )
    }

    #[test]
    fn sort_diag_test() {
        let sample = Line::from_str("9,7 -> 7,9");
        assert_eq!(
            Line {
                start: Point::new(7, 9),
                end: Point::new(9, 7)
            },
            sample
        )
    }

    #[test]
    fn forward_line_test() {
        let sample = Line::from_str("1,1 -> 1,3");
        assert_eq!(sample.points(), vec![(1, 1), (1, 2), (1, 3)])
    }

    #[test]
    fn reverse_line_test() {
        let sample = Line::from_str("9,7 -> 7,7");
        assert_eq!(sample.points(), vec![(7, 7), (8, 7), (9, 7)])
    }

    #[test]
    fn diagonal_line_test() {
        let sample = Line::from_str("1,1 -> 3,3");
        assert_eq!(sample.points(), vec![(1, 1), (2, 2), (3, 3)])
    }

    #[test]
    fn reverse_diagonal_line_test() {
        let sample = Line::from_str("9,7 -> 7,9");
        assert_eq!(sample.points(), vec![(7, 9), (8, 8), (9, 7)])
    }
}
