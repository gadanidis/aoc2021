use crate::utils::get_input;

fn get_fishes() -> Vec<usize> {
    get_input("day6.txt")
        .split(',')
        .map(|x| {
            x.trim()
                .parse::<usize>()
                .unwrap_or_else(|e| panic!("parse error on {}: {}", x, e))
        })
        .collect()
}

/// brute force
pub fn part1() -> usize {
    let mut fishes = get_fishes();

    for _ in 0..80 {
        let fishes2 = fishes.clone();
        for (i, f) in fishes2.iter().enumerate() {
            if f == &0 {
                fishes[i] = 6;
                fishes.push(8);
            } else {
                fishes[i] -= 1;
            }
        }
    }
    fishes.len()
}

/// more efficient
pub fn part2() -> usize {
    let fishes = get_fishes();

    let mut fish_list = [0; 9];
    for (i, fish) in fish_list.iter_mut().enumerate() {
        *fish = fishes.iter().filter(|f| f == &&i).count()
    }

    for _ in 0..256 {
        let spawning = fish_list[0];
        fish_list[0] = 0;
        for i in 0..8 {
            fish_list[i] = fish_list[i+1]
        }
        fish_list[6] += spawning;
        fish_list[8] = spawning;
    }
    fish_list.iter().sum()
}
