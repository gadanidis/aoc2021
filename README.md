# Advent of Code 2021

My solutions for Advent of Code 2021, working on learning Rust.

There are two branches, `main` and `optimized`.
My naïve solutions before looking at anyone else's answers are in the `main`
branch.
After solving a puzzle I like to look at others' solutions to see how mine can
be improved, which I put in the `optimized` branch.
